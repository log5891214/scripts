#! /bin/bash

if [ $# -ne 2 ];then
	echo "Veuillez fournir deux parametre: stop ou start et le nom du service"
	exit 8
fi

CHEMIN="/home/apx09/logs/gest_apache.log"

ETAT=$(systemctl is-active $2)

CRT_DATE=$(date "+%Y-%m-%d %H:%M:%S")

#Etat de demarage
if [ $1 = "start" ]; then

	if [ $ETAT = "active" ];then
		echo -e "log\n  ERROR: Tantative de demarage echouer \n  Cause: Service deja demarer \n  Date:  $CRT_DATE" >> $CHEMIN
		echo "Le service est deja demarer"
		exit 8
	else
		systemctl start $2
		ETSTR=$(systemctl is-active $2)
		if [ $ETSTR = "active" ];then
 	 		echo -e "log\n  INFO: Demarage du service avec succes \n  Date: $CRT_DATE " >> $CHEMIN
			echo "Le service vient d'etre demarer"
			exit 0
		else
			echo -e "log\n  FATAL: Tantative de demarage echouer \n  Cause: Erreur dans les elements du service \n  Date: $CRT_DATE" >> $CHEMIN
			echo "Le service n'a pas pu etre demarer"
			exit 8 
		fi
        fi

#Etat d'arret
elif [ $1 = "stop" ]; then

	if [ $ETAT = "inactive" ];then
		echo -e "log\n  ERROR: Tantative de d'arret echouer \n  Cause: Service deja arreter \n  Date:  $CRT_DATE" >> $CHEMIN
		echo "Le service est deja arreter" 
		exit 8
	else
		systemctl stop $2
                ETSTP=$(systemctl is-active $2)
                if [ $ETSTP = "inactive" ];then
                        echo -e "log\n  INFO: Arret du service avec succes \n  Date: $CRT_DATE" >> $CHEMIN
			echo "Le service vient d'etre arreter"
                        exit 0
               else
                      echo -e "log\n FATAL: Tantative d'arret echouer \n  Cause: Erreur dans les elements du service \n  Date: $CRT_DATE" >> $CHEMIN
		      echo "Le service  n'a pas pu etre arreter"
		      exit 8
                fi
	fi

#Autre Etat
else
	echo "votre premier parametre est different de stop et de start"
	exit 8
fi
