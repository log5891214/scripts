#!/bin/bash

if [ $# -ne 1 ];then
	echo "Veuillez fournir uniquement un seuille de consommation comme parametre"
	exit 1
fi

IFS=$'\n'

List=$(df -h | sed '1d' |awk '{print $5, $6}')

for fs in $List
do
	CONSO=$(echo $fs| awk '{print $1}' | tr -d "%")
	if [ $CONSO -ge $1 ];then
		NOM_MOUNT=$(echo $fs | awk '{print $2}')
		echo "le fs monter sur $NOM_MOUNT depasse le seuille, sa valeur est $CONSO"
	fi

done
