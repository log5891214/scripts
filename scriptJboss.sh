#!/bin/bash
if [ $# -ne 3 ]; then
	echo "le nombre de paramtres doit etre egale a 3, connection, application, serveur groupe"
	exit 1
fi
source "$1"
CHECK=$(/opt/wildfly/bin/jboss-cli.sh --connect --controller=$SERVER:$PORT_SRV --user=$USERNAME_J --password=$PASSWORD_J --command="ls server-group"|grep "$3")
IS_IN='false'
echo $CHECK
for i in $CHECK 
do
	if [ "$i" == "$3" ];then
     		IS_IN='true'
		echo $IS_IN
	fi
done
if [ $IS_IN == 'false' ];then
        /opt/wildfly/bin/jboss-cli.sh --connect --controller=$SERVER:$PORT_SRV --user=$USERNAME_J --password=$PASSWORD_J --command="/server-group=$3:add(profile=full, socket-binding-group=full-sockets"
        /opt/wildfly/bin/jboss-cli.sh --connect --controller=$SERVER:$PORT_SRV --user=$USERNAME_J --password=$PASSWORD_J --command="/host=slave2/server-config=srv-naim:add(group=$3)"
fi
/opt/wildfly/bin/jboss-cli.sh --connect --controller=$SERVER:$PORT_SRV --user=$USERNAME_J --password=$PASSWORD_J --command="deploy $2 --server-groups=$3"

echo $?
if [ $? -ne 0 ]; then
        /opt/wildfly/bin/jboss-cli.sh --connect --controller=$SERVER:$PORT_SRV --user=$USERNAME_J --password=$PASSWORD_J --command="deploy --name $2 --server-groups=$3 --force"
fi
